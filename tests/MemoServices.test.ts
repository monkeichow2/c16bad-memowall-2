import { MemoService } from "../services/MemoService";
import Knex from 'knex';
const KnexConfig = require("../knexfile");
const knex = Knex(KnexConfig["test"]);

describe("Test Memo Services",()=>{
    let memoService:MemoService;

    beforeAll(async()=>{
        await knex.seed.run();
    });

    beforeEach(()=>{
        memoService = new MemoService(knex);
    });

    test("Get All Memo",async()=>{
        const results = await memoService.getAllMemos();
        expect(results[0].content).toEqual("Memo#2");
        expect(results[1].content).toEqual("Memo#1");
        expect(results.length).toEqual(2);
    });

    test("New memo",async()=>{
        const results = await memoService.createMemo("Memo#3",undefined);
        const memos = await memoService.getAllMemos();
        expect(results).toEqual({ success: true });
        expect(memos.length).toEqual(3);
    });

    afterAll(async()=>{
        await knex.destroy();
    })
});
